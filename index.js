'use strict';

const
  express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  sonos = require('sonos-streams'),
  eventsWS = require('./server/events'),
  control = require('./server/control');

app.use(bodyParser.json());

function startZPSearch() {
  sonos.discovery.search();
  return sonos.discovery.zonePlayerStream
    .filter(device => device.mediaRenderer);
}

function setupSonosHandlers(zonePlayers) {
  eventsWS(zonePlayers, '/events', 8081);
  app.post('/control', control());
}

function serveStatic() {
  app.use(express.static(__dirname + '/.client'));
  app.get('/', function (req, res) {
    res.sendFile(__dirname + '/.client/static/index.html');
  });

  const server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('http server listening at http://%s:%s', host, port);
  });
}

const zonePlayers = startZPSearch();
setupSonosHandlers(zonePlayers);
serveStatic();

