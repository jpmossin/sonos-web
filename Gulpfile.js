'use strict';

const
  gulp = require('gulp'),
  browserify = require('browserify'),
  watchify = require('watchify'),
  babelify = require('babelify'),
  jscs = require('gulp-jscs'),
  gutil = require('gulp-util'),
  runSequence = require('run-sequence').use(gulp),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  $ = require('gulp-load-plugins')();

const conf = {
  CLIENT_DIST: '.client/',
  SERVER_SRC: './server/',
  CLIENT_SRC: './client/',
  ALL_JS: ['./client/app/js/**/*.js', './*.js', './server/*.js'],
  watch: true
};

gulp.task('default', function () {
  runSequence(['lint', 'stylecheck', 'sass', 'static', 'browserify'], function () {
    gulp.watch(conf.CLIENT_SRC + 'app/**/*.scss', ['sass']);
    gulp.watch(conf.ALL_JS, ['lint', 'stylecheck']);
  });
});

gulp.task('build', function () {
  conf.watch = false;
  runSequence(['lint', 'stylecheck', 'sass', 'static', 'browserify']);
});

gulp.task('static', function () {
  return gulp.src(conf.CLIENT_SRC + 'app/static/**/*')
    .pipe(gulp.dest(conf.CLIENT_DIST + 'static/'));
});

gulp.task('lint', function () {
  return gulp.src(conf.ALL_JS)
    .pipe($.react({errLogToConsole: true}))
    .pipe($.jshint('.jshintrc'))
    .pipe($.jshint.reporter('jshint-stylish'));
});

gulp.task('stylecheck', function () {
  return gulp.src(conf.ALL_JS)
    .pipe(jscs())
    .pipe(jscs.reporter());
});

gulp.task('sass', function () {
  return gulp.src(conf.CLIENT_SRC + './app/scss/main.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({errLogToConsole: true, imagePath: process.env.BASE_URL}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(conf.CLIENT_DIST + 'css/'));
});

gulp.task('browserify', function () {
  var browserifyArgs = watchify.args;
  browserifyArgs.debug = true;
  var bundler = browserify(conf.CLIENT_SRC + './app/js/app.js', browserifyArgs)
    .transform(babelify)
    .on('error', function (err) {
      gutil.log(gutil.colors.red('[Browserify Error]'), err.message);
    });

  function rebundle() {
    gutil.log(gutil.colors.yellow('[Browserify]'), 'Rebundling!');
    const startMillis = +new Date();
    return bundler
      .bundle()
      .on('error', function (err) {
        gutil.log(gutil.colors.red('[Browserify Error]'), err.message);
      })
      .pipe(source('bundle.js'))
      .pipe(buffer())
      .pipe(gulp.dest(conf.CLIENT_DIST))
      .on('finish', function () {
        const timeUsedMillis = (+new Date()) - startMillis;
        gutil.log(gutil.colors.yellow('[Browserify]'), 'Done [' + timeUsedMillis + ' ms]');
      });
  }

  if (conf.watch) {
    bundler = watchify(bundler);
    bundler.on('update', rebundle);
  }

  return rebundle();
});
