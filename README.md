Web-app for controlling sonos-devices. 
Built with React, RxJS, and sonos-streams.

Build with "npm install && gulp build" and start with "node index.js",
then access the controller on port 8080.