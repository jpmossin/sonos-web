'use strict';

const sonosControl = require('sonos-streams').control;

module.exports = function () {
  return function (req, resp) {
    const body = req.body;
    console.log('got req', body);
    sonosControl
      .executeAction(body.service, body.actionName, body.inArgs)
      .subscribe(
        actionResp => resp.send(actionResp),
        err => {
          console.error('action error', err);
          resp.status(400).end();
        });
  };
};

