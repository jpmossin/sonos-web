'use strict';

const
  sonos = require('sonos-streams'),
  WebSocketServer = require('ws').Server;

function eventKey(event) {
  return event.zpUDN + '-' + event.serviceTag;
}

const lastEvents = {};

module.exports = function (zonePlayers, path, port) {
  const eventsWs = new WebSocketServer({path: path, port: port});

  zonePlayers
    .flatMap(sonos.events.subscribe)
    .subscribe(subEvent => console.log('New subscription set up:', subEvent.zpUDN, subEvent.serviceTag));

  const zonePlayerEvents = zonePlayers
    .map(zp => ({
      data: zp,
      zpUDN: zp.UDN,
      serviceTag: 'zoneplayer'
    }));

  sonos.events.eventStream
    .merge(zonePlayerEvents)
    .subscribe(event => {
      lastEvents[eventKey(event)] = event;
      eventsWs.clients.forEach(client =>
          client.send(JSON.stringify(event)));
    });

  eventsWs.on('connection', function (ws) {
    Object.keys(lastEvents).forEach(eventKey => {
      ws.send(JSON.stringify(lastEvents[eventKey]));
    });
  });

};
