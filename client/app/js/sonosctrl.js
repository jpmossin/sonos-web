'use strict';

const React = require('react'),
  Player = require('./component/player/'),
  Topology = require('./component/topology/'),
  appStateStream = require('./state-stream/app-state').appStateStream,
  StreamListener = require('./state-stream/stream-listener');

const mainAppViews = {
  'topology': () => <Topology />,
  'player': () => <Player />,
  'browser': () => 'todo'
};

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    return appStateStream.
      map(appState => {
        const mainView = appState.mainView && appState.currentZG ? appState.mainView : 'topology';
        return {mainView: mainView};
      });
  },

  render() {
    return (
      <div id='sonosctrl'>
        {mainAppViews[this.state.mainView]()}
      </div>
    );
  }

});
