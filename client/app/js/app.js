'use strict';

const React = require('react'),
  SonosCtrl = require('./sonosctrl');

React.render(
  <SonosCtrl />, document.getElementById('app')
);
