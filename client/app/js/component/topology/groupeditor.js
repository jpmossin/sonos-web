'use strict';

const React = require('react'),
  topologyService = require('../../service/topology-service');

module.exports = React.createClass({

  getInitialState() {
    const selected = {};
    this.props.zg.members.forEach(zp => {
      selected[zp.UDN] = true;
    });
    return {selected: selected};
  },

  handleClick(player) {
    const seleced = this.state.selected;
    seleced[player.UDN] = !seleced[player.UDN];
    this.setState({selected: seleced});
  },

  handleDone() {
    const newGroupZPs = this.props.allPlayers.filter(zp => this.state.selected[zp.UDN]);
    topologyService.executeUpdate(this.props.zg.coordinator, this.props.zg.members, newGroupZPs);
    this.props.onDone();
  },

  renderCheckboxes() {
    return this.props.allPlayers.map(player =>
      <div onClick={this.handleClick.bind(this, player)} key={player.UDN}>
        <input type='checkbox' name='players'
               checked={this.state.selected[player.UDN]}>
          <span>{player.roomName}</span>
        </input>
      </div>
    );
  },

  render() {
    return (
      <div className='group-editor'>
        {this.renderCheckboxes()}
        <br />
        <button onClick={this.handleDone}>Done</button>
      </div>
    );
  }

});
