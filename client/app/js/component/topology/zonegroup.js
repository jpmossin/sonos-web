'use strict';

const React = require('react');

module.exports = React.createClass({

  handleEditClick() {
    this.props.setEditing(this.props.zg);
  },

  handleSelect() {
    this.props.setSelectedGroup(this.props.zg);
  },

  render() {
    const playerNodes = this.props.zg.members.map((player) =>
        (<li key={player.roomName}>{player.roomName}</li>)
    );
    return (
      <div className='zonegroup'>
        <a className='editgroup-link' href='#' onClick={this.handleEditClick}>edit</a>
        <a  href='#' onClick={this.handleSelect}>
          <ul>
          {playerNodes}
          </ul>
        </a>
      </div>
    );
  }

});
