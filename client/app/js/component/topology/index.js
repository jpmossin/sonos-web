'use strict';

const React = require('react'),
  zoneGroupStream = require('../../state-stream/zonegroup-state').zoneGroupStream,
  setAppState = require('../../state-stream/app-state').setAppState,
  StreamListener = require('../../state-stream/stream-listener'),
  ZoneGroup = require('./zonegroup'),
  GroupEditor = require('./groupeditor');

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    return zoneGroupStream
      .map(zoneGroups => ({zoneGroups: zoneGroups}));
  },

  setEditing(zg) {
    this.setState({editing: zg});
  },

  setSelectedGroup(zg) {
    setAppState({'currentZG': zg, mainView: 'player'});
  },

  renderZoneState() {
    const test = this.state.zoneGroups.map(zg =>
        <ZoneGroup
          setEditing={this.setEditing} zg={zg}
          key={zg.coordinator.UDN} setSelectedGroup={this.setSelectedGroup}/>
    );
    return <div className='zone-state'>{test}</div>;
  },

  render() {
    if (!this.state) {
      return <div>Loading zone state...</div>;
    }
    else if (this.state.zoneGroups.length === 0) {
      return <div>No zoneplayers found...</div>;
    }
    else if (this.state.editing) {
      const allPlayers = [].concat.apply([], this.state.zoneGroups.map(zg => zg.members));
      return <GroupEditor zg={this.state.editing}
                          className='group-editor'
                          onDone={() => this.setEditing(false)}
                          allPlayers={allPlayers}/>;
    }
    else {
      return this.renderZoneState();
    }
  }

});
