'use strict';

const
  React = require('react'),
  StreamListener = require('../../state-stream/stream-listener'),
  queueStream = require('../../state-stream/queue-state').queueStream,
  avtStream = require('../../state-stream/avt-state').avtStream,
  avtService = require('../../service/avt-service');

const QueueItem  = React.createClass({
  render () {
    const backgroundStyle = {backgroundColor: this.props.isCurrent ? '#eee' : '#fff'};
    const item = this.props.item;
    return (
      <div style={backgroundStyle}>{item.title} - {item.album} </div>
    );
  }
});

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    const queueState = queueStream.
      map(queueState => queueState[this.props.coordinator.UDN]);
    const currentItem = avtStream.
      map(avtAcc => ({currentTrack: avtAcc[this.props.coordinator.UDN].currentTrack}));
    return this.combineAndMerge(queueState, currentItem);
  },

  itemSelected(itemIndex) {
    avtService.seek(this.props.coordinator, 'TRACK_NR', itemIndex + 1);
  },

  render() {
    if (!this.state) {
      return <div>Loading queue...</div>;
    }

    const QueueItems = this.state.items.map((item, idx) =>
      <li key={idx} onClick={() => this.itemSelected(idx)}>
        <QueueItem item={item} isCurrent={idx === this.state.currentTrack - 1} />
      </li>);

    return (
      <div>
        <ul>
          {QueueItems}
        </ul>
      </div>
    );
  }

});
