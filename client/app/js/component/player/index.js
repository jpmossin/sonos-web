'use strict';

const React = require('react'),
  ActionBar = require('./actionbar'),
  CurrentlyPlaying = require('./currentlyplaying'),
  Queue = require('./queue'),
  {setAppState, appStateStream} = require('../../state-stream/app-state'),
  StreamListener = require('../../state-stream/stream-listener');

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    return appStateStream;
  },

  editZones() {
    setAppState({'currentZG': false});
  },

  render () {
    return (
      <div className='player'>
        <div>
          <a href='#' onClick={this.editZones}>{this.state.currentZG.zoneName}</a>
          <ActionBar currentZG={this.state.currentZG} />
          <CurrentlyPlaying coordinator={this.state.currentZG.coordinator} />
          <Queue coordinator={this.state.currentZG.coordinator} />
        </div>
      </div>
    );
  }

});

