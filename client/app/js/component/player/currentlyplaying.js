'use strict';

const
  React = require('react'),
  avtStream = require('../../state-stream/avt-state').avtStream,
  StreamListener = require('../../state-stream/stream-listener');

// Render one media-item / song
const SongInfo = React.createClass({

  renderItem() {
    return (<div> {this.props.label} {this.props.item.title} - {this.props.item.creator} </div>);
  },

  render() {
    var Info = this.props.item ? this.renderItem() : <i />;
    return (
      <div className='songinfo'> {Info} </div>
    );
  }
});

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    return avtStream
      .map(avtAcc => avtAcc[this.props.coordinator.UDN]);
  },

  renderInfo() {
    const avtState = this.state;
    return (
      <div className='currently-playing'>
        <SongInfo item={avtState.currentTrackMetaData} label=''/>
        <img src={'http://' + this.props.coordinator.hostUrl + avtState.currentTrackMetaData.albumArtURI} />
        <SongInfo item={avtState.nextTrackMetaData} label='Next:'/>
      </div>
    );
  },

  render: function () {
    console.log('curr play', this.state);
    return (
      this.state.currentTrackMetaData ?
        this.renderInfo() :
        <div> No music... </div>
    );
  }

});
