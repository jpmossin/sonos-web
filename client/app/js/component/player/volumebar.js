'use strict';

const
  React = require('react'),
  Rx = require('rx'),
  StreamListener = require('../../state-stream/stream-listener'),
  volumeService = require('../../service/volume-service'),
  volumeStream = require('../../state-stream/volume-state').volumeStream;

function computeGroupVolume(zoneGroup, volumes) {
  const summed = zoneGroup.members
    .map(zp => parseInt(volumes[zp.UDN].volume))
    .reduce((v1, v2) =>  v1 + v2, 0);
  return summed / zoneGroup.members.length;
}

// Put the volume adjustments on a stream so that we can throttle the requests
const volumeChanges = new Rx.Subject();
volumeChanges
  .sample(100)
  .distinctUntilChanged()
  .subscribe(({coord, vol}) =>
    volumeService.setGroupVolume(coord, vol));

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    return volumeStream
      .filter(() => !this.editing)  // skip when user is editing
      .map(volumeState =>
        ({groupVolume: computeGroupVolume(this.props.currentZG, volumeState)}));
  },

  handleVolumeChange(e) {
    const vol = e.target.value;
    const request = {coord: this.props.currentZG.coordinator, vol: vol};
    volumeChanges.onNext(request);
    this.setState({groupVolume: vol});
  },

  setEditing(editing) {
    this.editing = editing;
  },

  render () {
    return (
      <div className='volume-bar'>
        <i className='fa fa-lg fa-volume-up volume-icon' />
        <input type='range' className='custom' id='volume-bar' step='2' min='0' max='100'
               value={this.state.groupVolume} onChange={this.handleVolumeChange} onInput={this.handleVolumeChange}
               onMouseUp={() => this.setEditing(false)} onMouseDown={() => this.setEditing(true)} />
      </div>
    );
  }

});
