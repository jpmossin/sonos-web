'use strict';

const React = require('react'),
  StreamListener = require('../../state-stream/stream-listener'),
  avtService = require('../../service/avt-service'),
  VolumeBar = require('./volumebar'),
  avtStream = require('../../state-stream/avt-state').avtStream;

const transportStates = {
  playing: 'PLAYING',
  paused: 'PAUSED_PLAYBACK',
  stopped: 'STOPPED',
  transitioning: 'TRANSITIONING'
};

module.exports = React.createClass({

  mixins: [StreamListener],

  getStateStream() {
    return avtStream
      .map(avtState =>
        ({transportState: avtState[this.props.currentZG.coordinator.UDN].transportState}));
  },

  renderActionIcon(actionName, iconClass) {
    const action = () => avtService[actionName](this.props.currentZG.coordinator);
    const className = 'action-icon fa fa-lg ' + iconClass;
    return (
      <i className={className} onClick={action} />
    );
  },


  renderPlayPause() {
    const state = this.state.transportState;
    if (state === transportStates.paused || state === transportStates.stopped) {
      return this.renderActionIcon('play', 'fa-play');
    }
    else if (state === transportStates.playing) {
      return this.renderActionIcon('pause', 'fa-pause');
    }
    else if (state === transportStates.transitioning) {
      return <i className='action-icon fa fa-lg fa-spinner'></i>;
    }
    else {
      console.log('Unrecognized state', this.state.transportState);
    }
  },

  render () {
    return (
      <div className='action-bar'>
        <div className='action-icons'>
          {this.renderActionIcon('previous', 'fa-step-backward')}
          {this.renderPlayPause()}
          {this.renderActionIcon('next', 'fa-step-forward')}
        </div>
        <div>
          <VolumeBar currentZG={this.props.currentZG} />
        </div>
      </div>);
  }

});

