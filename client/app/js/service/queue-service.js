'use strict';

const control = require('./sonos-control');

function browse(service, queueId = 0, startingIndex = 0, requestedCount = 0) {
  const inArgs = {
    QueueID: queueId,
    StartingIndex: startingIndex,
    RequestedCount: requestedCount  // 0 = no limit
  };
  return control.executeAction(service, 'Browse', inArgs);
}

module.exports = {
  browse: browse
};
