'use strict';

const control = require('./sonos-control');

function adjustVolume(volume) {
  return Math.max(0, Math.min(80, volume));
}

function setVolume(zp, volume) {
  volume = adjustVolume(volume);
  const inArgs = {
    InstanceID: 0,
    Channel: 'Master',
    DesiredVolume: volume
  };
  control.executeAction(zp.mediaRenderer.renderingControl, 'SetVolume', inArgs);
}

function setGroupVolume(coordinator, volume) {
  volume = adjustVolume(volume);
  const inArgs = {
    InstanceID: 0,
    DesiredVolume: '' + volume
  };
  control.executeAction(coordinator.mediaRenderer.groupRenderingControl, 'SetGroupVolume', inArgs);
}

module.exports = {
  setVolume: setVolume,
  setGroupVolume: setGroupVolume
};

