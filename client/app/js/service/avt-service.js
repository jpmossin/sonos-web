'use strict';

const control = require('./sonos-control');

function getAVTService(zp) {
  return zp.mediaRenderer.avtransport;
}

function simpleAvtMsg(zp, actionName, inArgs) {
  const avt = getAVTService(zp);
  const _inArgs = inArgs ? inArgs : {
    InstanceID: 0
  };
  return control.executeAction(avt, actionName, _inArgs);
}

function play(zp) {
  return simpleAvtMsg(zp, 'Play', {
    InstanceID: 0,
    Speed: 1
  });
}

function pause(zp) {
  return simpleAvtMsg(zp, 'Pause');
}

function next(zp) {
  return simpleAvtMsg(zp, 'Next');
}

function previous(zp) {
  return simpleAvtMsg(zp, 'Previous');
}


function seek(zp, seekType, targetValue) {
  const avt = getAVTService(zp);
  const inArgs = {
    InstanceID: 0,
    Unit: seekType,
    Target: targetValue
  };
  return control.executeAction(avt, 'Seek', inArgs);
}

module.exports = {
  play: play,
  pause: pause,
  next: next,
  previous: previous,
  seek: seek
};

