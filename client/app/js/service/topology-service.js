'use strict';

const control = require('./sonos-control');

function leaveGroup(zp) {
  const inArgs = {InstanceID: 0};
  return control.executeAction(zp.mediaRenderer.avtransport, 'BecomeCoordinatorOfStandaloneGroup', inArgs);
}

function joinGroup(zp, groupCoordinator) {
  const coordURI = 'x-rincon:' + groupCoordinator.UDN.substr('uuid:'.length);
  const inArgs = {
    InstanceID: 0,
    CurrentURI: coordURI,
    CurrentURIMetaData: ''
  };
  return control.executeAction(zp.mediaRenderer.avtransport, 'SetAVTransportURI', inArgs);
}

/**
 * Send join and leave requests corresponding to
 * going from an original zone state to a new state.
 * @param coordinator group coordinator (of the current zone group)
 * @param currentZPs all players currently in the group.
 * @param newGroupZPs all players in the target-state group.
 */
function executeUpdate(coordinator, currentZPs, newGroupZPs) {
  if (newGroupZPs && newGroupZPs.length > 0) {
    const currentUDNs = new Set(currentZPs.map(zp => zp.UDN));
    const newGroupUDNs = new Set(newGroupZPs.map(zp => zp.UDN));
    const joiningPlayers = newGroupZPs.filter(zp => !currentUDNs.has(zp.UDN));
    const leavingPlayers = currentZPs.filter(zp => !newGroupUDNs.has(zp.UDN));
    const coordLeavingIndex = leavingPlayers.findIndex(zp => zp.UDN === coordinator.UDN);
    if (coordLeavingIndex >= 0) {
      leavingPlayers.splice(coordLeavingIndex, 1);
      window.setTimeout(() => leaveGroup(coordinator), 200);  // todo: think some more about this..
    }
    joiningPlayers
      .forEach(zp =>
        joinGroup(zp, coordinator)
          .subscribe(console.log(zp + ' joined new group'))
    );
    leavingPlayers.forEach(zp =>
      leaveGroup(zp)
        .subscribe(console.log(zp + ' left group'))
    );
    // todo: return merge stream of join/leave responses?
  }
}

module.exports = {
  executeUpdate: executeUpdate
};

