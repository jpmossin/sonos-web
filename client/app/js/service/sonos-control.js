'use strict';

const
  request = require('superagent'),
  Rx = require('rx');

function executeAction(service, actionName, inArgs) {
  const observable = Rx.Observable.create(obs => {
    request.post('/control')
      .set('Accept', 'application/json')
      .send({service: service, actionName: actionName, inArgs: inArgs})
      .end((err, res) => {
        if (err) {
          obs.onError(err);
        }
        else {
          obs.onNext(res.body);
        }
        obs.onCompleted();
      });
  }).publish();
  observable.connect();
  return observable;
}

module.exports = {
  executeAction: executeAction
};

