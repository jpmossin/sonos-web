'use strict';

/**
 * Parse the xml data of one media-item
 */
function parseMediaItemElem(mediaItemElem) {
  const resElem = mediaItemElem.getElementsByTagName('res')[0] || {};
  const tagValue = tagName => {
    const foundElem = mediaItemElem.getElementsByTagName(tagName)[0];
    return foundElem ? foundElem.textContent : null;
  };
  return {
    id: mediaItemElem.getAttribute('id'),
    protocolInfo: resElem.getAttribute('protocolInfo'),
    duration: resElem.getAttribute('duration'),
    mediaURI: resElem.textContent,
    albumArtURI: tagValue('albumArtURI'),
    title: tagValue('title'),
    creator: tagValue('creator'),
    album: tagValue('album'),
    itemClass: tagValue('class')
  };
}

module.exports = {
  parseMediaItemElem: parseMediaItemElem
};


