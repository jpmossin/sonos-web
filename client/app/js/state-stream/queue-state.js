'use strict';

const
  sonosEvents = require('./sonos-events'),
  domParser = new DOMParser(),
  {parseMediaItemElem} = require('./parse-util'),
  queueService = require('../service/queue-service');

function parseQueueLastChange(lastChangeData) {
  const queueIDElems = domParser.parseFromString(lastChangeData.lastChange, 'text/xml').children[0].children;
  const queueUpdateIDs = {};
  for (let i = 0; i < queueIDElems.length; i++) {
    const elem = queueIDElems[i];
    const queueId = elem.getAttribute('val');
    const updateId = elem.children[0].getAttribute('val');
    queueUpdateIDs[queueId] = updateId;
  }
  return queueUpdateIDs;
}

function parseBrowseResponse(browseResp) {
  const queueItemElems = domParser
    .parseFromString(browseResp.result, 'text/xml')
    .children[0].children;
  const queueItems = Array.prototype.slice.apply(queueItemElems)
    .map(parseMediaItemElem);
  return {
    items: queueItems,
    totalMatches: +browseResp.totalMatches,
    updateID: +browseResp.updateId
  };
}

// These events simply indicate that there has been an update to a queue (no queue data)
const queueChangeEvents = sonosEvents.eventStream
  .filter(event => event.serviceTag === 'queue')
  .map(event => ({
    zpUDN: event.zpUDN,
    service: event.service,
    data: parseQueueLastChange(event.data)
  }));

// Fetch queue data whenever there is a queue-change event
const queueStream = queueChangeEvents
  .flatMap(event =>
    queueService
      .browse(event.service, 0, 0)
      .map(resp => ({zpUDN: event.zpUDN, queue: parseBrowseResponse(resp.uBrowseResponse)})))
  .scan((acc, queueUpdate) => {
    acc[queueUpdate.zpUDN] = queueUpdate.queue;
    return acc;
  }, {})
  .replay(1);
queueStream.connect();


module.exports = {
  queueStream: queueStream
};
