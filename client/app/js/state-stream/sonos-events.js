'use strict';

const Rx = require('rx');

const ws = new WebSocket('ws://' + window.location.hostname + ':8081/events');

const eventStream = Rx.Observable.create(observer => {
  ws.onerror = e => observer.onError(e);
  ws.onclose = () => observer.onCompleted();
  ws.onmessage = msg => observer.onNext(JSON.parse(msg.data));
  return () => ws.close();
}).share();

module.exports = {
  eventStream: eventStream
};
