'use strict';

const
  sonosEvents = require('./sonos-events'),
  domParser = new DOMParser();

function parseRenderingLastChange(lastChangeData) {
  const instanceElems = Array.from(domParser.parseFromString(lastChangeData, 'text/xml').children[0].children);
  const instance0 = instanceElems.find(i => i.getAttribute('val') === '0');
  let volumeElems = Array.from(instance0.getElementsByTagName('Volume'));
  let isMuted = false;
  if (volumeElems.length === 0) {
    volumeElems = Array.from(instance0.getElementsByTagName('Mute'));
    isMuted = true;
  }
  const master = volumeElems.find(elem => elem.getAttribute('channel') === 'Master');
  return {
    volume: master.getAttribute('val'),
    isMuted: isMuted
  };
}

const volumeStream = sonosEvents.eventStream
  .filter(event => event.serviceTag === 'rendering')
  .map(event => ({zpUDN: event.zpUDN, data: parseRenderingLastChange(event.data.lastChange)}))
  .scan((acc, event) => {
    acc[event.zpUDN] = event.data;
    return acc;
  }, {})
  .shareReplay(1);

volumeStream.subscribe(() => null);

module.exports = {
  volumeStream: volumeStream
};


