'use strict';

const
  sonosEvents = require('./sonos-events'),
  domParser = new DOMParser();

const zonePlayerData = {};
sonosEvents.eventStream
  .filter(event => event.serviceTag === 'zoneplayer')
  .map(event => event.data)
  .subscribe(zp => {
    zonePlayerData[zp.UDN] = zp;
  });

function parseZoneGroupsData(zoneGroupsData) {

  function parseZoneGroup(zgElem) {
    const memberElems = Array.from(zgElem.children);
    const members = memberElems
      .map(member => zonePlayerData['uuid:' + member.getAttribute('UUID')])
      .filter(e => e);
    if (members.length === 0) {
      return null;
    }
    const coordUDN = 'uuid:' + zgElem.getAttribute('Coordinator');
    const coordinator = members.find(member => member.UDN === coordUDN);
    let zoneName = coordinator.roomName;
    if (members.length > 1) {
      zoneName += ' + ' + members.length;
    }

    return {
      coordinator: coordinator,
      zoneID: zgElem.getAttribute('ID'),
      members: members,
      zoneName: zoneName
    };
  }

  const zoneGroups = Array.from(domParser.parseFromString(zoneGroupsData, 'text/xml').children[0].children);
  return zoneGroups
    .map(parseZoneGroup)
    .filter(zg => zg);
}

const zoneGroupStream = sonosEvents.eventStream
  .filter(event => event.serviceTag === 'topology' && event.data.zoneGroupState)
  .map(event => parseZoneGroupsData(event.data.zoneGroupState))
  .shareReplay(1);

module.exports = {
  zoneGroupStream: zoneGroupStream
};
