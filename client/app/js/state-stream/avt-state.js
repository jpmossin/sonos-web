'use strict';

const
  sonosEvents = require('./sonos-events'),
  {parseMediaItemElem} = require('./parse-util'),
  domParser = new DOMParser();

// parse the xml-data of the lastChange event into a js object
function parseAVTLastChange(lastChangeData) {
  const avtStateElems = domParser.parseFromString(lastChangeData, 'text/xml').children[0].children[0].children;
  const avtProperties = {};
  for(let i = 0; i < avtStateElems.length; i++) {
    const elem = avtStateElems[i];
    const propertyName = elem.localName.substr(0, 1).toLowerCase() +  elem.localName.substr(1);
    avtProperties[propertyName] = elem.getAttribute('val');
  }
  const currentTrackMeta = (avtProperties.currentTrackMetaData || '').trim();
  const nextTrackMeta = (avtProperties.nextTrackMetaData || '').trim();
  avtProperties.currentTrackMetaData = currentTrackMeta.length > 0 ?
    parseMediaItemElem(domParser.parseFromString(avtProperties.currentTrackMetaData, 'text/xml').children[0].children[0]) :
    null;
  avtProperties.nextTrackMetaData = nextTrackMeta.length > 0 ?
    parseMediaItemElem(domParser.parseFromString(avtProperties.nextTrackMetaData, 'text/xml').children[0].children[0]) :
    null;

  return avtProperties;
}

const avtStream = sonosEvents.eventStream
  .filter(event => event.serviceTag === 'avt')
  .map(event => ({zpUDN: event.zpUDN, data: parseAVTLastChange(event.data.lastChange)}))
  .scan((acc, event) => {
    acc[event.zpUDN] = event.data;
    return acc;
  }, {})
  .replay(1);
avtStream.connect();

module.exports = {
  avtStream: avtStream
};
