'use strict';

// Subject-stream for setting "application wide" state,
// such as which zone group is selected.

const Rx = require('rx');

const appStateSubject = new Rx.ReplaySubject(1);

const currentState = {};
appStateSubject.onNext(currentState);

function setAppState(stateObj) {
  for (var key in stateObj) {
    if (stateObj.hasOwnProperty(key)) {
      currentState[key] = stateObj[key];
    }
  }
  appStateSubject.onNext(currentState);
}

module.exports = {
  appStateStream: appStateSubject,
  setAppState: setAppState
};


