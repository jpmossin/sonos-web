'use strict';

const Rx = require('rx'),
  _ = require('lodash');

/**
 * Component mixin for handling subscribing and
 * unsubscibing to streams upon mount and unmount.
 */
const StreamListener = {

  componentWillMount () {
    const stateStream = this.getStateStream(this.props)
      .filter(state => state !== null && typeof state !== 'undefined');
    this.__streamSubscription = stateStream.subscribe(obj => {
      this.setState(obj);
    });
  },

  componentWillUnmount () {
    this.__streamSubscription.dispose();
  },

  combineAndMerge (...observables) {
    return Rx.Observable.combineLatest(
      observables, _.merge.bind(null, {})
    );
  }

};

module.exports = StreamListener;
